# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 1.1.1

- patch: Internal maintenance: Upgrade dependency version bitbucket-pipes-toolkit.
- patch: Update the Readme with a new Atlassian Community link.
- patch: Update the Readme with details about default variables.

## 1.1.0

- minor: Add default values for AWS variables.

## 1.0.7

- patch: Internal maintenance: Add hadolint linter for Dockerfile

## 1.0.6

- patch: Add warning message about new version of the pipe available.

## 1.0.5

- patch: Internal maintenance: Add auto infrastructure for tests.

## 1.0.4

- patch: Fix readme example.

## 1.0.3

- patch: Internal release

## 1.0.2

- patch: Added code style checks

## 1.0.1

- patch: Fixed log's messages

## 1.0.0

- major: Parameter names were simplified

## 0.3.0

- minor: Fix the bug when one of the parameters was validated as required while it was actually optional

## 0.2.2

- patch: Fixed the bug with missing pipe.yml

## 0.2.1

- patch: Fixed the docker image

## 0.2.0

- minor: Changed pipe to use the full task definition json instead of only using the container definitions

## 0.1.0

- minor: Initial release

